from flask import Flask
from database.db import execute_query, connect
 
app = Flask(__name__)

@app.route('/')
def index():
    return  'Hello World!'
    

@app.route('/<string:name>/<string:location>')
def save(name, location):
    return execute_query('insert', name, location)


@app.route('/<string:name>')
def fetch(name):
    return execute_query('select', name)


@app.route('/conectar')
def conectar():
    return connect()