FROM python:3.8

RUN pip3 install pipenv
RUN pip3 install uwsgi
WORKDIR /app
COPY Pipfile Pipfile.lock ./
RUN pipenv install --system --deploy --ignore-pipfile
COPY . .
CMD [ "uwsgi","app.ini" ]